---
title: "Using Hugo on Gitlab"
date: 2019-12-01T01:01:01+01:00
draft: false
tags: [Hugo, gitlab, cloudflare]
---

[TOC]

## Install Hugo
Goto Hugo's github page and download https://github.com/gohugoio/hugo or install from your OS distribution.

## Creating new site
```
$ hugo new site mysite
```

## Configure theme

## Configure config.toml
We only need to apply some basic configuration. Simply replace the content of `config.toml` with the following:

## About page
```
$ hugo new about.md
```

## Add first post
```
$ hugo new post/first.md
```

```
+++
date = "2019-01-01"
title = "first"
slug = "first" 
tags = []
categories = []
series = []
+++
```

## Local preview

```
$ hugo server 
```

If you wanna see drafts in preview, use
```
$ hugo server -D 
```
Then visit http://localhost:1313/ in your web browser.

##  References
* https://tkainrad.dev/posts/using-hugo-gitlab-pages-and-cloudflare-to-create-and-run-this-website/